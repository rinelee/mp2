package ca.ubc.ece.eece210.mp2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {
	private ArrayList<Element> catalogue;

	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		catalogue = new ArrayList<Element>();
	}
	
	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 * @throws FileNotFoundException 
	 * 			if the file specified could not be found.
	 */
	public Catalogue(String fileName) throws FileNotFoundException {
		catalogue = new ArrayList<Element>();
		Scanner readCLog = new Scanner(new File(fileName));
		String currentLine = "";
		
		while(readCLog.hasNextLine()){
			currentLine = readCLog.nextLine().trim();
			
			if(currentLine.equals("<*>")){ 
				currentLine = readCLog.nextLine().trim();
				
				String restoreStringForm = "";
				while(readCLog.hasNextLine() && !currentLine.equals("</*>")){
					restoreStringForm += currentLine.trim() + "\n";
					currentLine = readCLog.nextLine().trim();
				}
				
				catalogue.add(Genre.restoreCollection(restoreStringForm.trim()));
			}
		}
		
		readCLog.close();
		
	}

	/**
	 * Adds a genre to the catalogue
	 * 
	 * @requires 
	 * 		The genre is not null and not a sub-genre.
	 * @param genre
	 * 		The genre to add to the catalogue.
	 */
	public void addRootGenre(Genre genre){
		catalogue.add(genre);
	}
	
	/**
	 * Adds an album to the catalogue
	 * 
	 * @requires
	 * 		The album is not null and not a part of any genre.
	 * @param album
	 * 		The album to be added to the catalogue.
	 */
	
	public void addRootAlbum(Album album){
		catalogue.add(album);
	}
	
	/**
	 * Saves the contents of the catalogue to the given file.
	 * 
	 * @param fileName 
	 * 			the file where to save the library
	 * @throws IOException 
	 * 			if the file cannot be written to.
	 */
	public void saveCatalogueToFile(String fileName) throws IOException {
		PrintWriter saveCLog = new PrintWriter(fileName);
		
		for(Element rootItem: catalogue){
				saveCLog.println("<*>");
				saveCLog.println(rootItem.toString());
				saveCLog.println("</*>");
		}
		
		saveCLog.close();
		
	}
	
	/**
	 * Saves the contents of the catalogue to a string.
	 * 
	 * @return
	 * 		String form of the catalogue.
	 */
	public String toString(){
		String stringRep = "";
		
		for(Element rootItem: catalogue){
			stringRep +=("<*>\n");
			stringRep += rootItem.toString().trim() + "\n";
			stringRep += ("</*>\n");
		}
		
		return stringRep.trim();
		
	}
	
	/**
	 * Reads a catalogue data file and reproduces its contents as a string.
	 * 
	 * @param fileName
	 * 			the name of the file to read.
	 * @return
	 * 			the contents of the file as a string.
	 * @throws FileNotFoundException
	 * 			if the file provided cannot be found.
	 */
	public static String readCatalogueFile(String fileName) throws FileNotFoundException{
		Scanner readCLog = new Scanner(new File(fileName));
		String fileContents = "";
		
		while(readCLog.hasNextLine()){
			fileContents += readCLog.nextLine() + "\n";
		}
		
		readCLog.close();
		
		return fileContents.trim();
		
	}
	
}

