package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Genre extends Element {
	private static HashMap<String, ArrayList<Genre>> allGenres = new HashMap<String, ArrayList<Genre>>(); //Keeps a running list of existing genres to prevent duplicates.
	private static boolean enableGenreRepeat = false; //Controls whether duplicates can be added or not.
	private final String genreName;

	/**
	 * Creates a new genre with the given name if the
	 * genre does not already exist. Sub-genres can be
	 * deleted and recreated, but if a genre has no parent
	 * genre, it will always exist even if it is not 
	 * referenced.
	 * 
	 * Note: Genre names are case sensitive, therefore
	 * "Name" and "name" would be considered unique
	 * genres.
	 * 
	 * @requires 
	 * 			name is not null.
	 * @throws IllegalArgumentException
	 * 			 if only special characters other than "!" 
	 * 			 and "-" are provided, name is an empty string, 
	 * 			 or name already corresponds to an existing genre while 
	 * 			 duplicate genres are disabled.
	 * @param name
	 *            the name of the genre.
	 */
	public Genre(String name) {
		
		String cleanName = name.replaceAll("[^a-zA-Z0-9!-]+", "").trim(); //Removes any special characters.
		
		if(cleanName.isEmpty() || (!enableGenreRepeat && allGenres.containsKey(cleanName))){ //Check for name validity.
			throw new IllegalArgumentException();
		}
		
		genreName = cleanName;
		
		if(allGenres.containsKey(genreName)){ //If repeat is enabled, keep track of the number of duplicates of a genre.
			ArrayList<Genre> listOfGenre = allGenres.get(genreName);
			listOfGenre.add(this);
			allGenres.put(genreName, listOfGenre);
		}
		
		else{
			ArrayList<Genre> listOfGenre = new ArrayList<Genre>();
			listOfGenre.add(this);
			allGenres.put(genreName, listOfGenre);
		}
	
	
	}	
	
	/**
	 * Returns the name of the genre.
	 * 
	 * @return
	 * 		name of the genre.
	 */
	public String getName(){
		return genreName;
	}
	
	/**
	 * Restores a genre from its given string representation.
	 * 
	 * @param stringRepresentation
	 * 				a genre and its sub-genres written in the mark-up language generated.
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		Queue<String> insideGenre = new LinkedList<String>(); //Saves any sub-genres or albums inside the genre to a queue.
		
		Scanner stringRepReader = new Scanner(stringRepresentation);
		String name = stringRepReader.nextLine().replaceAll("[^a-zA-Z0-9!-]+", "");
		Genre genreToRestore = new Genre(name); //First create the main genre being restored.
		
		while(stringRepReader.hasNextLine()){
			String currentLine = stringRepReader.nextLine().trim(); 
			
			if(!currentLine.replaceAll("[^a-zA-Z0-9!-]+", "").equals(name)){ //Check to see if the genre has nothing inside. If so we are finished.
				insideGenre.add(currentLine); //Saves all the inside content to the queue.
			}
		}
		
		while(!insideGenre.isEmpty()){ 
			String nextValue = insideGenre.remove().trim(); //Obtain the first item inside.
			
			//Case 1: nextValue is an album.
			if(nextValue.startsWith("<a>") && nextValue.endsWith("</a>")){ //Check that it's not just a genre called "a"
				genreToRestore.addChild(new Album(nextValue));  //Albums are one line and created differently.
			}
				
			//Case 2: nextValue is the start of a genre with no sub-elements.
			else if(nextValue.replaceAll("[^a-zA-Z0-9!-]+", "").equals(insideGenre.peek().replaceAll("[^a-zA-Z0-9!-]+", ""))){
				genreToRestore.addToGenre(new Genre(nextValue.replaceAll("[^a-zA-Z0-9!-]+", ""))); 
				insideGenre.remove(); //Genres exist on two lines, so closing for genre tag is needed too.
			}
			
			//Case 3: nextValue is the start of a genre with sub-elements.
			else{
				String newStringRep = nextValue; //Create a string to save all the contents of the genre, including the genre tags.
				
				while(!nextValue.replaceAll("[^a-zA-Z0-9!-]+", "").equals(insideGenre.peek().replaceAll("[^a-zA-Z0-9!-]+", ""))){
					//Retrieve everything inside this genre.
					newStringRep += "\n" + insideGenre.remove();
				}
				
				newStringRep += "\n" + insideGenre.remove(); //Retrieve the closing genre tag.

				genreToRestore.addToGenre(restoreCollection(newStringRep.trim())); //Recursively restore what is inside.
			}
		}
		
		
		stringRepReader.close();
		return genreToRestore;
		
		
	}

	/**
	 * Returns the string representation of a genre
	 * 
	 * @return
	 * 		string representation of genre.
	 */
	public String toString() {
		
		String stringRep = "<" + genreName + ">"; //Opening brackets are <genreName>
		
		if(!this.getChildren().isEmpty()){
			for(Element children: this.getChildren()){ //Recursively obtain all elements inside the genre.
				stringRep += "\n";
				stringRep += children.toString(); 
			}
		}
		
		stringRep += "\n</" + genreName + ">"; //Closing brackets are </genreName>
		
		return stringRep;
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @requires
	 * 			 b is not null.
	 * @param b
	 *            the element to be added to the collection.
	 */
	public void addToGenre(Element b) {
		if(!b.hasChildren()){ //If b is an album ensure that album also notes the genre.
			Album albumToAdd = (Album)b;
			albumToAdd.addToGenre(this);
		}
		
		else{
			this.addChild(b);
		}
	}
	

	/**
	 * Removes the given album or genre to from genre
	 * 
	 * @requires 
	 * 			  b is not null.
	 * @param b
	 *            the element to be removed from the collection if
	 *            it is a child of the genre. Otherwise does
	 *            nothing.
	 */
	public void removeFromGenre(Element b){
		
		if(b.hasChildren() && allGenres.containsKey(b.getName()) && this.getChildren().contains(b)){ //Checks that the element is a sub-genre of the genre.
			String elementToAddName = b.getName();
			
			if(allGenres.get(elementToAddName).size() > 1){
				
				ArrayList<Genre> listOfGenre = allGenres.get(b.getName());
				listOfGenre.remove(this);
				allGenres.put(elementToAddName, listOfGenre);
			}
			
			else{
				allGenres.remove(elementToAddName);
			}
			
		}
		else if(!b.hasChildren()){
			Album albumToRemove = (Album)b;
			
			if(albumToRemove.getGenre().equals(this)){
				albumToRemove.removeFromGenre();
			}
		
		}
		else{
			removeChild(b); 
		}
		
	}
	
	/**
	 * Allows or disallows duplicate genres. Generally duplicates should not be 
	 * enabled outside of testing.
	 * 
	 * <p><b>Warning:</b> duplicate genres added while they are permitted will 
	 * not disappear when duplicate genres are disallowed. Only future 
	 * duplicates will be prevented.</p>
	 * 
	 * @param status
	 * 			true if duplicate genres are to be enabled,
	 * 			or false if duplicates are disabled.
	 * 
	 */
	public static void enableRepeat(boolean status){
		enableGenreRepeat = status;
	}
	
	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}

	/**
	 * Searches for a retrieves a genre of a given name and its 
	 * order of creation (for duplicate genres).
	 * 
	 * @param genreName
	 * 			The name of the genre to search for.
	 * @param genreIndex
	 * 			The order in which the genre was created in, for duplicate genres.
	 * @return
	 * 			The genre specified by name and creation order.
	 */
	public static Genre searchAndRetrieveGenre(String genreName, int genreIndex) {
		return allGenres.get(genreName).get(genreIndex);
	}

	/**
	 * 
	 * @param genre
	 * 			The genre to retrieve the order of creation of.
	 * @return
	 * 			The order the genre was created in, for duplicates. If only one genre
	 * 			exists, this value is 1. If it is the third duplicate to be created,
	 * 			then this value is 3.
	 */
	public static int retrieveGenreIndex(Genre genre) {
		return allGenres.get(genre.getName()).indexOf(genre);
	}

}