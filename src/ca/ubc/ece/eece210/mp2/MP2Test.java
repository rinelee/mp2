package ca.ubc.ece.eece210.mp2;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

public class MP2Test {
	
	public Album createFateAlbum(){
		String title = "Fate/Stay Night";
		String Artist = "Type-Moon";
		ArrayList<String> songs = new ArrayList<String>();
		
		songs.add("Ever Present Feeling");
		songs.add("Sorrow");
		songs.add("This Illusion");
		songs.add("Ideal White");
		
		return new Album(title, Artist, songs);
		
	}
	
	//Determine if duplicate genres are correctly blocked 
	@SuppressWarnings("unused")
	@Test
	public void test0(){
		Genre.enableRepeat(false);
		
		try{
			Genre genre1 = new Genre("Witch of the Holy Night");
			Genre genre2 = new Genre("Witch of the Holy Night");
			
			fail("Should have thrown exception.");
		}
		catch(IllegalArgumentException e){
			
		}
		
	}

	//Add album to and from a genre
	@Test
	public void test1() {
		Genre.enableRepeat(true); //Allow duplicate genres to be created for testing purposes.
		Album album1 = createFateAlbum();
		Genre genre1 = new Genre("Visual Novels");
		
		album1.addToGenre(genre1);
		assertTrue(genre1.getChildren().contains(album1));

		album1.removeFromGenre();
		assertTrue(!genre1.getChildren().contains(album1));
		
		genre1.addToGenre(album1);
		assertTrue(genre1.getChildren().contains(album1));
		
	}
	
	
	//Save an Album to string form and recreate it using string form.
	@Test
	public void test2(){
		Genre.enableRepeat(true);
		Album album1 = createFateAlbum();
		String stringForm1 = album1.toString();
		
		Album album2 = new Album(stringForm1);
		
		assertEquals(stringForm1, album2.toString());
	
	}
	
	//Save simple and complex genres to and from string form.
	@Test
	public void test3(){
		Genre.enableRepeat(true);
		Genre genre1 = new Genre("Visual novels");
		Genre genre2 = Genre.restoreCollection(genre1.toString());
		
		assertEquals(genre1.toString(), genre2.toString());
		
		Genre genre3 = new Genre("FSN");
		genre1.addToGenre(genre3);
		
		Album album1 = createFateAlbum();
		genre3.addToGenre(album1);
		
		genre2 = Genre.restoreCollection(genre1.toString());
		assertEquals(genre1.toString(), genre2.toString());
		
		Genre genre4 = new Genre("Mahoyo");
		genre1.addToGenre(genre4);
		
		Genre genre5 = new Genre("2014");
		genre3.addChild(genre5);
		
		genre2 = Genre.restoreCollection(genre1.toString());
		assertEquals(genre1.toString(), genre2.toString());
		
	}
	
	//Test catalogue creation
	@Test
	public void test4(){
		Genre.enableRepeat(true);
		Catalogue catalogue1 = new Catalogue();
		Genre genre1 = new Genre("Visual Novels");
	
		Genre genre3 = new Genre("FSN");
		genre1.addToGenre(genre3);
		
		Album album1 = createFateAlbum();
		genre3.addToGenre(album1);
		
		Genre genre5 = new Genre("2014");
		genre3.addChild(genre5);
		
		catalogue1.addRootGenre(genre1);
		catalogue1.addRootGenre(genre3);
		
		try {
			catalogue1.saveCatalogueToFile("TypeMoonMusic.txt");
		} catch (IOException e) {
			fail("Exception should not be thrown.");
		}
		
		Catalogue catalogue2 = null;
		
		try {
			catalogue2 = new Catalogue("TypeMoonMusic.txt");
		} catch (FileNotFoundException e) {
			fail("File should have been written from before.");
		}
		
		try {
			catalogue2.saveCatalogueToFile("TypeMoonMusic2.txt"); //Compare files to determine if they are the expected output for reference.
		} catch (IOException e) {
			fail("Exception should not have been thrown.");
		}
		
		//Attempt to make a catalogue from a non-existing file.
		
		try{
			catalogue2 = new Catalogue("no file.txt");
			fail("Exception should have been thrown.");
		}
		catch (FileNotFoundException e) {
		}
		
		//Confirm the two catalogues are the same.
		assertEquals(catalogue1.toString(), catalogue2.toString());
		
		//Confirm that file creation was correct.
		try {
			assertEquals(catalogue1.toString(), Catalogue.readCatalogueFile("TypeMoonMusic.txt"));
		} catch (FileNotFoundException e) {
			fail("Exception should not have been thrown.");
		}
		
	}
	
	//Test genre inclusion & other glass box tests.
	@Test
	public void test5(){
		Genre.enableRepeat(true);
		
		Genre genre1 = new Genre("Visual Novels");
		Genre genre2 = new Genre("Fate");
		Genre genre3 = new Genre("Tsukihime");
		Genre genre4 = new Genre("Fate/Stay Night");
		Genre genre5 = new Genre("Fate/Extra");
		Genre genre6 = new Genre("Mahoyo");
		Genre genre7 = new Genre("Fate/Hollow Ataraxia");
		Album album1 = createFateAlbum();
		Album album2 = createFateAlbum();
		
		//Case 1: Multiple elements of one genre.
		genre1.addToGenre(album1);
		genre1.addChild(genre2);
		
		assertTrue(genre1.getChildren().contains(album1));
		assertTrue(genre1.getChildren().contains(genre2));
		
		//Case 2: Add an element to a sub-genre.
		genre2.addToGenre(genre4);
		genre2.addToGenre(album2);
		
		assertTrue(genre2.getChildren().contains(genre4));
		assertTrue(genre2.getChildren().contains(album2));
		
		//Case 3: Trying to move an album with a genre without removing first.
		try{
			genre2.addToGenre(album1);
			fail("Did not throw an exception.");
		}
		catch(IllegalArgumentException e){
			
		}
		
		try{
			genre2.addChild(album1);
			fail("Did not throw an exception.");
		}
		catch(IllegalArgumentException e){
			
		}
		
		//Case 4: Move an album after removing first.	
		try{
			genre1.removeFromGenre(album1);
			genre4.addToGenre(album1);
		}
		catch(IllegalArgumentException e){
			fail("Exception should not have been thrown.");
		}
		
		assertTrue(genre4.getChildren().contains(album1));
		
		//Case 5: Trying to add a genre that's already a sub-genre to a genre.
		genre2.addToGenre(genre5);
		genre2.addToGenre(genre7);
		genre1.addToGenre(genre3);
		genre1.addToGenre(genre6);
		Album album3 = createFateAlbum();
		
		genre7.addToGenre(album3);
		
		try{
			genre1.addToGenre(genre7);
			fail("Exception should have been thrown.");
		}
		catch(IllegalArgumentException e){
		}
	
		//Case 6: Varying (large) iteration length for string form conversion.
		/*
		 * Tests for:
		 * 	-A genre with only an album inside
		 * 	-A genre with multiple elements inside
		 * 	-A genre with multiple elements with multiple elements inside
		 * 	-A leaf genre
		 */
		
		Genre testGenre = Genre.restoreCollection(genre1.toString());
		assertEquals(testGenre.toString(), genre1.toString());
		
	}
	
	//Test boundary conditions of creating an album.
	@SuppressWarnings("unused")
	@Test
	public void test6(){
		ArrayList<String> emptySongs = new ArrayList<String>();
		try{
			Album album1 = new Album("Code Geass", "Sunrise", null);
			Album album2 = new Album("Code Geass", "Sunrise", emptySongs);
		}
		catch(NullPointerException e){
			fail("Exception should not have been thrown.");
		}

	}
	
	
	//Test boundary conditions of creating an genre.
	@SuppressWarnings("unused")
	@Test
	public void test7(){
		try{
			Genre genre1 = new Genre("");
			fail("Should have thrown exception.");
		}
		catch(IllegalArgumentException e){
		}
		
		try{
			Genre genre2 = new Genre("*~*");
			fail("Should have thrown exception.");
		}
		catch(IllegalArgumentException e){
		}

	}
	
}
