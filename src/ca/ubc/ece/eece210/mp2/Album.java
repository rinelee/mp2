package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 * This class contains the information needed to represent 
 * an album in our application.
 * 
 */

public final class Album extends Element {
	private final ArrayList<String> albumSongs;
	private final String title;
	private final String performer;
	private Genre albumGenre;
	
	/**
	 * Builds an album with the given title, performer and song list. 
	 * *Note: Special characters aside from '!' and '-' are ignored,
	 * including spaces.
	 * 
	 * @param title
	 *            the title of the album
	 * @param author
	 *            the performer 
	 * @param songlist
	 * 			  the list of songs in the album
	 */
	public Album(String title, String performer, ArrayList<String> songlist) {
		albumSongs = new ArrayList<String>();
		this.title = title.replaceAll("[^a-zA-Z0-9!-]+", "");
		this.performer = performer.replaceAll("[^a-zA-Z0-9!-]+", "");
		
		if(songlist != null && !songlist.isEmpty()){ //Saves songs if there are any.
			for(String songs: songlist){
				albumSongs.add(songs.replaceAll("[^a-zA-Z0-9!-]+", ""));
			}
		}

	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * 
	 * @requires 
	 * 		stringRepresentation is in the correct format for an album and the album 
	 * 		has at least one song. The genre of the album, if not null, must exist.
	 * 	
	 * @param stringRepresentation
	 *            the string representation
	 */
	public Album(String stringRepresentation) {
		albumSongs = new ArrayList<String>();
		Scanner stringRepReader = new Scanner(stringRepresentation);
		
		title = stringRepReader.next().trim().replaceAll("<a>", "");
		performer = stringRepReader.next().trim(); 
		
		String genreName = stringRepReader.next().trim();
		int genreIndex = Integer.parseInt(stringRepReader.next());
		
		if(genreIndex != 0){ //Ensure that the album actually has a genre.
			genreIndex -= 1;
			albumGenre = Genre.searchAndRetrieveGenre(genreName, genreIndex);
		}
		
		while(stringRepReader.hasNext()){
			albumSongs.add(stringRepReader.next().trim().replaceAll("</a>","")); 	
		}
		
		stringRepReader.close();
		
	}

	/**
	 * Returns the string representation of the given album. The representation
	 * contains the title, performer and songlist, as well as the genre and its order of
	 * creation (in case of duplicates) that the album belongs to.
	 * 
	 * <p><b>Warning:</b> It is assumed that duplicate sub-genres will not be removed after the
	 * string has been created to preserve string accuracy. To preserve accuracy
	 * after such a removal, the string must be generated again.</p>
	 * 
	 * @return 
	 * 		the string representation.
	 */
	public String toString() {
		int genreIndex = 0;
		String stringRep;
		
		if(albumGenre != null){
			genreIndex = Genre.retrieveGenreIndex(albumGenre) + 1;
			stringRep = "<a>" + title + " " + performer + " " +  albumGenre.getName() + " " + genreIndex;
		}
		else{
			stringRep = "<a>" + title + " " + performer + " " +  albumGenre + " " + genreIndex;
		}
		
		
		for(String song: albumSongs){
			stringRep += " " + song;
		}
		
		stringRep += "</a>"; //Saves in format
		
		return stringRep;
		
	}
	
	/**
	 * Add the album to the given genre
	 * 
	 * 
	 * @throws IllegalArgumentException
	 * 				if the album already has a genre, or b is not of type 
	 * 				genre.
	 * @param b
	 *            the genre to add the album to.
	 */
	public void addToGenre(Element b) {
		if (!b.hasChildren()){
			throw new IllegalArgumentException("Cannot add an album to an album.");
		}
		if(albumGenre != null){
			throw new IllegalArgumentException("The album already has a genre.");
		}
		
		albumGenre = (Genre) b;
		albumGenre.addChild(this);
	
	}
	
	/**
	 * 
	 * @effect
	 * 		Removes the album its current genre if 
	 * 		it has a genre.
	 * 
	 */
	public void removeFromGenre(){
		if(albumGenre != null){
			albumGenre.removeChild(this);
		}
		
		albumGenre = null;
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to
	 */
	public Genre getGenre() {
		return albumGenre;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return 
	 * 		the performer.
	 */
	public String getPerformer() {
		return performer;
	}
	
	/**
	 * Returns the title of the album.
	 * 
	 * @return	
	 * 		title of the album.
	 */
	public String getName(){
		return title;
	}
	

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
	
}

