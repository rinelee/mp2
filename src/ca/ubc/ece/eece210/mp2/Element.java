package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class to represent an entity in the catalogue. The element (in
 * this implementation) can either be an album or a genre.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public abstract class Element {
	private ArrayList<Element> elementList = new ArrayList<Element>();
	private boolean hasParent = false;
	

	/**
	 * Returns all the children of this entity. They can be albums or genres. In
	 * this particular application, only genres can have children. Therefore,
	 * this method will return the albums or genres contained in this genre.
	 * 
	 * @return 
	 * 		the children
	 */
	public List<Element> getChildren() {
		return new ArrayList<Element>(elementList);
	}

	/**
	 * Adds a child to this entity. Basically, it is adding an album or genre to
	 * an existing genre
	 * 
	 * @requires
	 * 			b is not null
	 * @throws IllegalArgumentException
	 * 			  if the entity does not accept children.
	 * @param b
	 *            the entity to be added.
	 */
	protected void addChild(Element b) {
		if(hasChildren() && !b.hasParentStatus()){
			elementList.add(b);
			b.setHasParent(true);
		}
		
		else{
			throw new IllegalArgumentException("The element you want to add is already a part of another.");
		}
	
	}
	
	/**
	 * Removes a child from this entity.
	 * 
	 * @requires
	 * 			  b is not null.
	 * @param b
	 *            the entity to be removed.
	 * @effect
	 * 			  b is removed from the entity if it is a child 
	 * 			  of the entity. Otherwise does nothing.
	 */
	protected void removeChild(Element b) {
		
		if(elementList.contains(b)){
			elementList.remove(b);
			b.setHasParent(false);
		}
		
	}
	
	/**
	 * Sets the parent indicator to the specified boolean. Used
	 * to determine if the entity is a child of another entity.
	 * 
	 * @param b
	 * 		The boolean to set.
	 */
	private void setHasParent(boolean b){
		hasParent = b;
	}
	
	/**
	 * Returns whether or not the element is the sub-element of a genre.
	 * 
	 * @return
	 * 		true if the element has a parent, false otherwise.
	 */
	public boolean hasParentStatus(){
		return hasParent;
	}

	/**
	 * Abstract method returning the title or name of an entity.
	 * 
	 * @return
	 * 		the name or title of the entity.
	 */
	public abstract String getName();

	/**
	 * Abstract method to determine if a given entity can (or cannot) contain
	 * any children.
	 * 
	 * @return 
	 * 		true if the entity can contain children, or false otherwise.
	 */
	public abstract boolean hasChildren();

}